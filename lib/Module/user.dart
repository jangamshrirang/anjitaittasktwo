


class UserDetails {
  final int? id;
  final String? name, email, gender;

  UserDetails({
    this.id,
    this.name,
    this.email,
    this.gender,
    // 6%2F02%2Fc0%2Fzuckheadsho.a33d0.jpg'
  });

  factory UserDetails.fromJson(Map<dynamic, dynamic> json) {
    return new UserDetails(
        id: json["id"],
        name: json["name"],
        email: json["email"],
        gender: json["gender"]);
  }
}