import 'dart:io';

import 'package:flutter/material.dart';

import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:task/screen/signup.dart';
import 'package:task/service/storageutil.dart';
import './constant/app_style.dart';
// import 'package:flutter/material.dart';
import 'package:flutter_offline/flutter_offline.dart';
import 'package:path_provider/path_provider.dart';
import 'package:hive/hive.dart';
import 'package:hive_flutter/hive_flutter.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await StorageUtil.getInstance();
  Directory document = await getApplicationDocumentsDirectory();
  Hive.init(document.path);
  await Hive.openBox<String>("users");
  runApp(
      // DevicePreview(
      // builder: (context) =>
      MyApp()
      // )

      );
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return ScreenUtilInit(
      designSize: Size(360, 690),
      // allowFontScaling: false,
      builder: () => MaterialApp(
        debugShowCheckedModeBanner: false,
        title: 'Flutter_ScreenUtil',
        theme: ThemeData(
            // primarySwatch: Colors.cyan,
            primaryColor: bgColor),
        home: SignUp(),
      ),
    );
  }
}

// class DemoPage extends StatelessWidget {
//   @override
//   Widget build(BuildContext context) {
//     return new Scaffold(
//       appBar: new AppBar(
//         title: new Text("Offline Demo"),
//       ),
//       body: OfflineBuilder(
//         connectivityBuilder: (
//           BuildContext context,
//           ConnectivityResult connectivity,
//           Widget child,
//         ) {
//           final bool connected = connectivity != ConnectivityResult.none;
//           return new Stack(
//             fit: StackFit.expand,
//             children: [
//               Positioned(
//                 height: 24.0,
//                 left: 0.0,
//                 right: 0.0,
//                 child: Container(
//                   color: connected ? Color(0xFF00EE44) : Color(0xFFEE4400),
//                   child: Center(
//                     child: Text("${connected ? 'ONLINE' : 'OFFLINE'}"),
//                   ),
//                 ),
//               ),
//               Center(
//                 child: new Text(
//                   'Yay!',
//                 ),
//               ),
//             ],
//           );
//         },
//         child: Column(
//           mainAxisAlignment: MainAxisAlignment.center,
//           children: <Widget>[
//             new Text(
//               'There are no bottons to push :)',
//             ),
//             new Text(
//               'Just turn off your internet.',
//             ),
//           ],
//         ),
//       ),
//     );
//   }
// }

// import 'package:flutter/material.dart';
// import 'package:flutter/services.dart';
// import 'package:task/searchcall.dart';
// import 'package:task/service/networkrepo.dart';
// // import 'package:flutter_search/search.dart';
// import './namesearch.dart';

// void main() {
//   WidgetsFlutterBinding.ensureInitialized();
//   SystemChrome.setEnabledSystemUIOverlays([]);
//   runApp(MyApp());
// }

// class MyApp extends StatelessWidget {
//   @override
//   Widget build(BuildContext context) {
//     return MaterialApp(
//       title: 'SearchDelegate',
//       theme: ThemeData(
//         primarySwatch: Colors.blue,
//         visualDensity: VisualDensity.adaptivePlatformDensity,
//       ),
//       home: HomePage(),
//       // MyHomePage(title: 'SearchDelegate'),
//       debugShowCheckedModeBanner: false,
//     );
//   }
// }

// class MyHomePage extends StatefulWidget {
//   MyHomePage({Key? key, this.title}) : super(key: key);

//   final String? title;

//   @override
//   _MyHomePageState createState() => _MyHomePageState();
// }

// class _MyHomePageState extends State<MyHomePage> {
//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//       appBar: AppBar(
//         title: Text(widget.title!),
//       ),
//       body: Container(),
//       // ListView.builder(
//       //   itemCount: names.length,
//       //   itemBuilder: (BuildContext context, int index) {
//       //     return ListTile(
//       //       title: Text(
//       //         names.elementAt(index),
//       //       ),
//       //     );
//       //   },
//       // ),
//       floatingActionButton: FloatingActionButton(
//         child: Icon(Icons.search),
//         onPressed: () async {
//           var res = await API_Manager().sigUp();
//           // for(){

//           // }
//           // print(res["data"]["name"]);
//           var repo = res["data"]["name"];

//           for (var item in repo) {
//             print(item["name"]);
//           }

// //           showSearch(context: context, delegate: NameSearch(
// // // name:
// //           ));
//           // final result = await showSearch<String>(
//           //   context: context,
//           //   delegate: NameSearch(),
//           // );

//           // print(result);
//         },
//       ),
//     );
//   }
// }
