import 'dart:async';

// import 'package:filter_listview_example/api/books_api.dart';
// import 'package:filter_listview_example/main.dart';
// import 'package:filter_listview_example/model/book.dart';
// import 'package:filter_listview_example/widget/search_widget.dart';
import 'package:flutter/material.dart';
import 'package:task/Module/user.dart';
import 'package:task/service/networkrepo.dart';
import '../service/searchwidget.dart';

class FilterNetworkListPage extends StatefulWidget {
  @override
  FilterNetworkListPageState createState() => FilterNetworkListPageState();
}

class FilterNetworkListPageState extends State<FilterNetworkListPage> {
  List<UserDetails> users = [];
  String query = '';
  Timer? debouncer;

  @override
  void initState() {
    super.initState();

    init();
  }

  @override
  void dispose() {
    debouncer?.cancel();
    super.dispose();
  }

  void debounce(
    VoidCallback callback, {
    Duration duration = const Duration(milliseconds: 1000),
  }) {
    if (debouncer != null) {
      debouncer!.cancel();
    }

    debouncer = Timer(duration, callback);
  }

  Future init() async {
    final users = await API_Manager.getUsers(query);

    setState(() => this.users = users);
  }

  @override
  Widget build(BuildContext context) => Scaffold(
        appBar: AppBar(
          title: Text('Search'),
          centerTitle: true,
        ),
        body: Column(
          children: <Widget>[
            buildSearch(),
            Expanded(
              child: ListView.builder(
                itemCount: users.length,
                itemBuilder: (context, index) {
                  final book = users[index];

                  return buildBook(book);
                },
              ),
            ),
          ],
        ),
      );

  Widget buildSearch() => SearchWidget(
        text: query,
        hintText: 'Title or Author Name',
        onChanged: searchBook,
      );

  Future searchBook(String query) async => debounce(() async {
        final books = await API_Manager.getUsers(query);

        if (!mounted) return;

        setState(() {
          this.query = query;
          this.users = books;
        });
      });

  Widget buildBook(UserDetails user) => ListTile(
      
        title: Text(user.name!),
        subtitle: Text(user.email!),
        trailing: Text(user.gender!),
      );
}
