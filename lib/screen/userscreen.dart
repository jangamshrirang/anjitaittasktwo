import 'package:flutter/material.dart';
import 'package:flutter_offline/flutter_offline.dart';
import 'package:task/Module/usermodule.dart';
import 'package:task/service/networkrepo.dart';
import 'package:connectivity/connectivity.dart';
import 'dart:async';
// import 'dart:io';

// import 'package:connectivity/connectivity.dart';
import 'package:flutter/foundation.dart';
// import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:task/service/storageutil.dart';
import 'package:hive/hive.dart';
import 'package:hive_flutter/hive_flutter.dart';

class UserDetails extends StatefulWidget {
  const UserDetails({Key? key}) : super(key: key);

  @override
  _UserDetailsState createState() => _UserDetailsState();
}

class _UserDetailsState extends State<UserDetails> {
  Box<String>? friendsBox;
  Future<UserModule>? _feeModel;
  bool? connected;
  var connectivityResult;
  chaeck() async {
    connectivityResult = await (Connectivity().checkConnectivity());
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    friendsBox = Hive.box<String>("users");
  }

  @override
  Widget build(BuildContext context) {
    _feeModel = API_Manager().getUSerData();

    return Scaffold(
      appBar: new AppBar(
        title: new Text("Offline"),
        actions: [
          RaisedButton(onPressed: () {
            print(StorageUtil.getListString('userInfo'));
          })
        ],
      ),
      body: OfflineBuilder(
        connectivityBuilder: (
          BuildContext context,
          ConnectivityResult connectivity,
          Widget child,
        ) {
          // setState(() {
          connected = connectivity != ConnectivityResult.none;
          // });

          return connected!
              ? FutureBuilder<UserModule>(
                  future: _feeModel,
                  builder: (context, snapshot) {
                    if (snapshot.hasData) {
                      return ListView.builder(
                        itemCount: snapshot.data!.data!.length,
                        itemBuilder: (context, index) {
                          //  final key = snapshot.data!.data![1].name!;

                          // final value = snapshot.data!.data![1].name!;
                          // friendsBox!.put(key, value);
                          StorageUtil.putListString('userInfo', [
                            snapshot.data!.data![0].name!,
                            snapshot.data!.data![0].email!,
                            snapshot.data!.data![0].gender!,
                            snapshot.data!.data![1].name!,
                            snapshot.data!.data![1].email!,
                            snapshot.data!.data![1].gender!,
                            snapshot.data!.data![2].name!,
                            snapshot.data!.data![2].email!,
                            snapshot.data!.data![2].gender!,
                            snapshot.data!.data![3].name!,
                            snapshot.data!.data![3].email!,
                            snapshot.data!.data![3].gender!,
                            snapshot.data!.data![4].name!,
                            snapshot.data!.data![4].email!,
                            snapshot.data!.data![4].gender!,
                          ]);
                          // StorageUtil.putListString('userInf', [
                          //   _feeModel.toString()
                          //   // snapshot.data!.data![1].name!,
                          //   // snapshot.data!.data![1].email!
                          //   // response[0]['usertype'],
                          //   // response[0]['username']
                          // ]);
                          return Card(
                            child: Column(
                              children: [
                                SizedBox(
                                  height: 10,
                                ),
                                Text(
                                  snapshot.data!.data![index].name!,
                                  style: TextStyle(fontWeight: FontWeight.bold),
                                ),
                                Text(snapshot.data!.data![index].email!),
                                Text(snapshot.data!.data![index].gender!),
                                SizedBox(
                                  height: 10,
                                )
                              ],
                            ),
                          );
                        },
                      );
                    } else if (snapshot.hasError) {
                      return Column(
                        children: [
                          Icon(
                            Icons.error_outline,
                            color: Colors.red,
                            size: 60,
                          ),
                          Center(
                            child: Padding(
                              padding: const EdgeInsets.only(top: 16),
                              child: Text('Error: Data not Available'),
                            ),
                          )
                        ],
                      );
                      //   [

                      // ];
                    } else {
                      return Column(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          Center(
                            child: SizedBox(
                              child: CircularProgressIndicator(),
                              width: 60,
                              height: 60,
                            ),
                          ),
                          Center(
                            child: const Padding(
                              padding: EdgeInsets.only(top: 16),
                              child: Text('Awaiting result...'),
                            ),
                          )
                        ],
                      );
                      //   <Widget>[

                      // ];
                    }

                    // );
                  },
                )
              :
              // ValueListenableBuilder(
              //     valueListenable: friendsBox!.listenable(),
              //     builder: (context, Box<String> friends, _) {
              //       return ListView.separated(
              //           itemBuilder: (context, index) {
              //             final key = friends.keys.toList()[index];
              //             final value = friends.get(key);

              //             return ListTile(
              //               title: Text(
              //                 value!,
              //                 style: TextStyle(
              //                     fontWeight: FontWeight.bold, fontSize: 22),
              //               ),
              //               subtitle: Text(key,
              //                   style: TextStyle(
              //                       fontWeight: FontWeight.bold, fontSize: 18)),
              //             );
              //           },
              //           separatorBuilder: (_, index) => Divider(),
              //           itemCount: friends.keys.toList().length);
              //     },
              //   );

              ListView.builder(
                  itemCount: StorageUtil.getListString('userInfo').length,
                  itemBuilder: (context, index) {
                    return Card(
                        child: Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Center(
                          child: Text(
                              StorageUtil.getListString('userInfo')[index])),
                    ));
                  });
        },
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            new Text(
              'There are no bottons to push :)',
            ),
            new Text(
              'Just turn off your internet.',
            ),
          ],
        ),
      ),
    );
  }
}
