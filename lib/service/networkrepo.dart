import 'package:http/http.dart' as http;
import 'package:task/Module/user.dart';

import 'dart:convert';

import 'package:task/Module/usermodule.dart';

class API_Manager {
  Future signUp({
    String? name,
    String? gender,
    String? email,
    String? token,
  }) async {
    print(name);

    var url = Uri.parse('https://gorest.co.in/public-api/users');

    var response = await http.post(
      url,
      body: {
        'name': '$name',
        'gender': '$gender',
        'email': '$email',
        'status': 'Active',
      },
      headers: {
        // "Content-Type": "application/json",
        "Accept": "application/json",
        "Authorization": "Bearer " + token!
      },
    );
    print('Response status: ${response.statusCode}');
    print('Response body: ${response.body}');
    var convertDatatoJson = jsonDecode(response.body);
    return convertDatatoJson;
  }

  Future<UserModule> getUSerData() async {
    var feeModel;

    try {
      var response =
          await http.get(Uri.parse("https://gorest.co.in/public-api/users"));

      if (response.statusCode == 200) {
        var jsonString = response.body;
        print(jsonString);
        var jsonMap = json.decode(jsonString);
        feeModel = UserModule.fromJson(jsonMap);
      }
    } catch (Execption) {
      return feeModel;
    }
    return feeModel;
  }

  Future getUserData() async {
    var feeModel;
    var jsonMap;
    try {
      var response =
          await http.get(Uri.parse("https://gorest.co.in/public-api/users"));

      if (response.statusCode == 200) {
        var jsonString = response.body;
        print(jsonString);
        jsonMap = json.decode(jsonString);
        print(jsonMap);
        // feeModel = UserModule.fromJson(jsonMap);
      }
    } catch (Execption) {
      return jsonMap;
    }
    return jsonMap;
  }

  static Future<List<UserDetails>> getUsers(String query) async {
    final url = Uri.parse('https://gorest.co.in/public-api/users');
    final response = await http.get(url);

    if (response.statusCode == 200) {
      final responseJson = json.decode(response.body);
      print(responseJson);
      var res = responseJson["data"];
      final List users = res;

      return users.map((json) => UserDetails.fromJson(json)).where((users) {
        final titleLower = users.name!.toLowerCase();
        final authorLower = users.email!.toLowerCase();
        final searchLower = query.toLowerCase();

        return titleLower.contains(searchLower) ||
            authorLower.contains(searchLower);
      }).toList();
    } else {
      throw Exception();
    }
  }
}
